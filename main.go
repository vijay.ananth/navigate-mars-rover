package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

// Rover will have its co-ordinates and direction to move on receiving command
type Rover struct {
	X         int
	Y         int
	Direction int
	Plateau   Plateau
}

//Mars Plateau will have co-ordinates
type Plateau struct {
	X int
	Y int
}

var directionMap = map[string]int{
	"N": 0,
	"E": 1,
	"S": 2,
	"W": 3,
}

func (r *Rover) move() error {
	switch r.Direction {
	case 0:
		if r.Y >= r.Plateau.Y {
			return errors.New("Invalid direction received")
		}
		r.Y++
	case 1:
		if r.X >= r.Plateau.X {
			return errors.New("Invalid direction received")
		}
		r.X++
	case 2:
		if r.Y <= 0 {
			return errors.New("Invalid direction received")
		}
		r.Y--
	case 3:
		if r.X <= 0 {
			return errors.New("Invalid direction received")
		}
		r.X--
	}
	fmt.Println("Rover's current position is ", r.X, r.Y, r.Direction)
	return nil
}

func (r *Rover) processMessage(message string) error {
	switch {
	case message == "M":
		fmt.Println("message received is ", message)
		return r.move()
	case message == "R" || message == "L":
		fmt.Println("message received is ", message)
		r.changeDirection(message)
		return nil
	default:
		return errors.New("Invalid message")
	}
}

func (r *Rover) changeDirection(direction string) {
	switch direction {
	case "L":
		r.Direction--
	case "R":
		r.Direction++
	}
	if r.Direction > 3 {
		r.Direction = 0
	}
	if r.Direction < 0 {
		r.Direction = 3
	}
}

func main() {

	var directions, intput string
	var err error
	fmt.Println("Hello Mars")

	fmt.Println("Enter Plauteau's max Co-ordinates in X,Y format")
	fmt.Scanln(&intput)
	plateauCoOrdinates := strings.Split(intput, ",")
	plateau := Plateau{}
	plateau.X, err = strconv.Atoi(plateauCoOrdinates[0])
	plateau.Y, err = strconv.Atoi(plateauCoOrdinates[1])

	fmt.Println("Enter Rover's current Co-ordinates and direction in X,Y,D format")
	fmt.Scanln(&intput)
	roversCoOrdinates := strings.Split(intput, ",")
	rover := Rover{}
	rover.X, err = strconv.Atoi(roversCoOrdinates[0])
	rover.Y, err = strconv.Atoi(roversCoOrdinates[1])
	rover.Direction = directionMap[roversCoOrdinates[2]]
	rover.Plateau = plateau

	fmt.Println("Enter directions")
	fmt.Scanln(&directions)

	for _, ch := range directions {
		err = rover.processMessage(string(ch))
		if err != nil {
			fmt.Println(err.Error())
			break
		}
	}

	fmt.Println("current location of rover", rover)
	fmt.Println(err)
}
